﻿using JHR_GetIcon;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using zhuxunzui_WindowsFileManager.main.core;
using zhuxunzui_WindowsFileManager.main.util;
namespace zhuxunzui_WindowsFileManager
{
    public partial class zxz_attr_form : Form
    {
        private string fullName;
        public zxz_attr_form(string fullPath)
        {
            InitializeComponent();
            fullName = fullPath;
        }

        private void zxz_attr_form_Load(object sender, EventArgs e)
        {
            string type = "";
            string createTime = "";
            string size = "";
            //string total = "";
            string subObjs = "-";
            string parentLocation = "";
            string fileName = "";

            if (Directory.Exists(fullName))
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(fullName);
                fileName = directoryInfo.Name;
                type = "文件夹";
                DirectoryInfo[] infos = directoryInfo.GetDirectories();
                FileInfo[] fileInfos = directoryInfo.GetFiles();
                subObjs = infos.Count() + "文件夹， " + fileInfos.Count() + " 文件";

                long sizes = FolderHelPer.totalSize(fullName);

                pictureBox1.Image = IconHelper.getImage_B().Images[Const.DEFAULTFOLDER];



                size = FolderHelPer.getSizeString(fullName);
                createTime = directoryInfo.LastWriteTime.ToString();
            }
            else if (File.Exists(fullName))
            {
                FileInfo fileInfo = new FileInfo(fullName);
                fileName = fileInfo.Name;
                type = new GetIcon().GetTypeName(fullName);

                size = FolderHelPer.changeFileSizeFormatToString(fileInfo.Length);
                createTime = fileInfo.LastWriteTime.ToString();
                pictureBox1.Image = IconHelper.getImage_B().Images[ListViewControler.GetFileIconKey((string)(new GetIcon().GetTypeName(fullName)), fullName)];
            }
            parentLocation = Directory.GetParent(fullName).FullName;

            fileSize_label.Text = size;
            fileName_label.Text = fileName;
            fileLocalcation_label.Text = parentLocation;
            fileCreateTime_label.Text = createTime;
            fileObjs_label.Text = subObjs;
            file_type_label.Text = type;
            fileTotal_label.Text = size;

        }
    }
}
