﻿namespace zhuxunzui_WindowsFileManager
{
    partial class zxz_attr_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.fileName_label = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.fileCreateTime_label = new System.Windows.Forms.Label();
            this.fileObjs_label = new System.Windows.Forms.Label();
            this.fileTotal_label = new System.Windows.Forms.Label();
            this.fileSize_label = new System.Windows.Forms.Label();
            this.fileLocalcation_label = new System.Windows.Forms.Label();
            this.file_type_label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(51, 23);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // fileName_label
            // 
            this.fileName_label.AutoSize = true;
            this.fileName_label.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.fileName_label.Location = new System.Drawing.Point(155, 50);
            this.fileName_label.Name = "fileName_label";
            this.fileName_label.Size = new System.Drawing.Size(76, 21);
            this.fileName_label.TabIndex = 1;
            this.fileName_label.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "类型：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(50, 154);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "位置：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(50, 184);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "大小：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(50, 215);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "占用空间：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(50, 246);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 12);
            this.label6.TabIndex = 6;
            this.label6.Text = "包含:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(50, 280);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 7;
            this.label7.Text = "创建时间：";
            // 
            // fileCreateTime_label
            // 
            this.fileCreateTime_label.AutoSize = true;
            this.fileCreateTime_label.Location = new System.Drawing.Point(130, 280);
            this.fileCreateTime_label.Name = "fileCreateTime_label";
            this.fileCreateTime_label.Size = new System.Drawing.Size(41, 12);
            this.fileCreateTime_label.TabIndex = 13;
            this.fileCreateTime_label.Text = "label8";
            // 
            // fileObjs_label
            // 
            this.fileObjs_label.AutoSize = true;
            this.fileObjs_label.Location = new System.Drawing.Point(130, 246);
            this.fileObjs_label.Name = "fileObjs_label";
            this.fileObjs_label.Size = new System.Drawing.Size(41, 12);
            this.fileObjs_label.TabIndex = 12;
            this.fileObjs_label.Text = "label9";
            // 
            // fileTotal_label
            // 
            this.fileTotal_label.AutoSize = true;
            this.fileTotal_label.Location = new System.Drawing.Point(130, 215);
            this.fileTotal_label.Name = "fileTotal_label";
            this.fileTotal_label.Size = new System.Drawing.Size(47, 12);
            this.fileTotal_label.TabIndex = 11;
            this.fileTotal_label.Text = "label10";
            // 
            // fileSize_label
            // 
            this.fileSize_label.AutoSize = true;
            this.fileSize_label.Location = new System.Drawing.Point(130, 184);
            this.fileSize_label.Name = "fileSize_label";
            this.fileSize_label.Size = new System.Drawing.Size(47, 12);
            this.fileSize_label.TabIndex = 10;
            this.fileSize_label.Text = "label11";
            // 
            // fileLocalcation_label
            // 
            this.fileLocalcation_label.AutoSize = true;
            this.fileLocalcation_label.Location = new System.Drawing.Point(130, 154);
            this.fileLocalcation_label.Name = "fileLocalcation_label";
            this.fileLocalcation_label.Size = new System.Drawing.Size(47, 12);
            this.fileLocalcation_label.TabIndex = 9;
            this.fileLocalcation_label.Text = "label12";
            // 
            // file_type_label
            // 
            this.file_type_label.AutoSize = true;
            this.file_type_label.Location = new System.Drawing.Point(130, 122);
            this.file_type_label.Name = "file_type_label";
            this.file_type_label.Size = new System.Drawing.Size(47, 12);
            this.file_type_label.TabIndex = 8;
            this.file_type_label.Text = "label13";
            // 
            // zxz_attr_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(352, 337);
            this.Controls.Add(this.fileCreateTime_label);
            this.Controls.Add(this.fileObjs_label);
            this.Controls.Add(this.fileTotal_label);
            this.Controls.Add(this.fileSize_label);
            this.Controls.Add(this.fileLocalcation_label);
            this.Controls.Add(this.file_type_label);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.fileName_label);
            this.Controls.Add(this.pictureBox1);
            this.Name = "zxz_attr_form";
            this.Text = "zxz_attr_form";
            this.Load += new System.EventHandler(this.zxz_attr_form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label fileName_label;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label fileCreateTime_label;
        private System.Windows.Forms.Label fileObjs_label;
        private System.Windows.Forms.Label fileTotal_label;
        private System.Windows.Forms.Label fileSize_label;
        private System.Windows.Forms.Label fileLocalcation_label;
        private System.Windows.Forms.Label file_type_label;
    }
}