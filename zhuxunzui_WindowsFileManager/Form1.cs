﻿using System;
using System.Collections;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using zhuxunzui_WindowsFileManager.main.controler;
using zhuxunzui_WindowsFileManager.main.core;
using zhuxunzui_WindowsFileManager.main.util;
namespace zhuxunzui_WindowsFileManager
{
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }
        
        private void splitContainer1_SizeChanged(object sender, EventArgs e) {
            combo_url.Width = splitContainer1.Panel1.Width - pbtn_next.Location.X - pbtn_next.Width-10;
        }

        private void Form1_Load(object sender, EventArgs e) {

            //imageViewShow ima = new imageViewShow();
            //ima.Show();


            Combo_urlControler.getCombo_urlControlerObject(combo_url);

            StatusToolBarHelper.setObjectNum(lb_objectNum);
            TreeNodeControler.setTreeView(treeView1);
            TreeNodeControler.init();

            //imageList2 = IconHelper.getImage_B();
            //imageList1 = IconHelper.getImage_S();

            ListViewControler.setListView(listView1);
            ListViewControler.init();

            pbtn_pre.Click += new EventHandler(HistoryControler.preAccessPath);
            pbtn_next.Click += new EventHandler(HistoryControler.nextAccessPath);

            reflush_btn.Click += new EventHandler(ListViewControler.reflushListView);
            parent_node_btn.Click += new EventHandler(ListViewControler.returnParentNode);

            ContextMuneStripController.setContextMenuStrip(mouseRightMenu, listView1);
            
        }

        private void splitContainer2_Panel2_SizeChanged(object sender, EventArgs e) {
            listView1.Width = splitContainer2.Panel2.Width- 2;
            listView1.Height = splitContainer2.Panel2.Height - 2;

        }

        

        private void ToolStripMenuItem_Click(object sender, ToolStripItemClickedEventArgs e) {
            
            ToolStripSplitButton tsb = (ToolStripSplitButton) sender;
            for (int i = 0; i < tsb.DropDownItems.Count; i++) {
                if (tsb.DropDownItems[i] != e.ClickedItem)
                    ((ToolStripMenuItem)tsb.DropDownItems[i]).Checked = false;
                else ((ToolStripMenuItem)tsb.DropDownItems[i]).Checked = true;
            }

            switch (e.ClickedItem.Text) {
                case "大图标": listView1.View = View.LargeIcon;
                    break;
                case "小图标": listView1.View = View.SmallIcon;
                    break;
                case "列表": listView1.View = View.List;
                    break;
                case "详细列表": listView1.View = View.Details;


                    break;
                default: listView1.View = View.Tile;
                    break;
            }
        }

        private void listView1_MouseEnter(object sender, EventArgs e) {
            listView1.ContextMenuStrip = mouseRightMenu;
        }

        private void fileSearch_btn_Click(object sender, EventArgs e) {
            if (textBox1.Text.Trim().Equals("")) {
                //combo_url_SelectedIndexChanged(null, null);
                return;
            }
            toolStripStatusLabel2.Visible = true;
            if (combo_url.Text.Equals(Const.RECYCLE))//回收站只搜索当前目录，不搜索子目录
            {
                for (int i = listView1.Items.Count - 1; i >= 0; i--) {
                    string temp = listView1.Items[i].SubItems[0].Text.ToUpper();
                    if (temp.IndexOf(textBox1.Text.Trim().ToUpper()) == -1) listView1.Items.RemoveAt(i);
                }
            } else //搜索符合条件的文件，同时搜索所有子目录
            {
                //lb_searching2.Visible = true;
               // lb_ojbnum.Text = "0";
                //statusStrip1.Refresh();//刷新显示，让正在搜索中的文字得已显示



                //******************************************底部文字的显示
                listView1.Items.Clear();
                ListViewControler.CreateCol_R();
                string topfolder = combo_url.Text;
                if (topfolder.Equals("我的电脑")) {
                    DriveInfo[] drives = DriveInfo.GetDrives();
                    foreach (DriveInfo drive in drives)
                        if (drive.IsReady) ListViewControler.doSearchFile(drive.Name, textBox1.Text.Trim());
                } else
                    if (topfolder.Equals("收藏夹")) {
                        ListViewControler.doSearchFile(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), textBox1.Text.Trim());
                        ListViewControler.doSearchFile(Environment.GetFolderPath(Environment.SpecialFolder.MyMusic), textBox1.Text.Trim());
                        ListViewControler.doSearchFile(Environment.GetFolderPath(Environment.SpecialFolder.MyPictures), textBox1.Text.Trim());
                        ListViewControler.doSearchFile(Environment.GetFolderPath(Environment.SpecialFolder.MyVideos), textBox1.Text.Trim());
                    } else
                        ListViewControler.doSearchFile(topfolder, textBox1.Text.Trim());
            }
            StatusToolBarHelper.setObjectNum(listView1.Items.Count.ToString());
            toolStripStatusLabel2.Visible = false;
            //lb_ojbnum.Text = listView1.Items.Count.ToString();
            //lb_searching2.Visible = false;
        }
        #region 自定义变量
        int currentCol = -1;
        bool sort;
        #endregion

        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e) {
            string Asc = ((char)0x25bc).ToString().PadLeft(4, ' ');
            string Des = ((char)0x25b2).ToString().PadLeft(4, ' ');

            if (sort == false) {
                sort = true;
                string oldStr = this.listView1.Columns[e.Column].Text.TrimEnd((char)0x25bc, (char)0x25b2, ' ');
                this.listView1.Columns[e.Column].Text = oldStr + Des;
            } else if (sort == true) {
                sort = false;
                string oldStr = this.listView1.Columns[e.Column].Text.TrimEnd((char)0x25bc, (char)0x25b2, ' ');
                this.listView1.Columns[e.Column].Text = oldStr + Asc;
            }
            //if (listView1.Columns[e.Column].Text.Contains("")) {

            //}
            listView1.ListViewItemSorter = new ListViewItemComparer(e.Column, sort,listView1.Columns[e.Column].Text);
            
            this.listView1.Sort();
            int rowCount = this.listView1.Items.Count;
            if (currentCol != -1) {
                for (int i = 0; i < rowCount; i++) {
                    this.listView1.Items[i].UseItemStyleForSubItems = false;
                    this.listView1.Items[i].SubItems[currentCol].BackColor = Color.White;

                    if (e.Column != currentCol)
                        this.listView1.Columns[currentCol].Text = this.listView1.Columns[currentCol].Text.TrimEnd((char)0x25bc, (char)0x25b2, ' ');
                }
            }

            for (int i = 0; i < rowCount; i++) {
                this.listView1.Items[i].UseItemStyleForSubItems = false;
                this.listView1.Items[i].SubItems[e.Column].BackColor = Color.WhiteSmoke;
                currentCol = e.Column;
            }
        }

        public class ListViewItemComparer : IComparer {
            public bool sort_b;
            public SortOrder order = SortOrder.Ascending;

            private int col;

            private string columName;
            public ListViewItemComparer() {
                col = 0;
            }

            public ListViewItemComparer(int column, bool sort, string colName) {
                col = column;
                sort_b = sort;
                columName = colName;
            }

            public int Compare(object x, object y) {
                if (columName.Contains("大小")) {
                    long sizex = 0;
                    try {
                        if (File.Exists(((ListViewItem)x).Tag.ToString()))
                            sizex = new FileInfo(((ListViewItem)x).Tag.ToString()).Length;
                        else {
                            if (col < 3)
                                sizex = new DriveInfo(((ListViewItem)x).Tag.ToString()).TotalSize;
                            else sizex = new DriveInfo(((ListViewItem)x).Tag.ToString()).TotalFreeSpace;
                        }
                    }
                    catch {
                        sizex = 0;
                    }
                    long sizey = 0;
                    try {
                        if (File.Exists(((ListViewItem)y).Tag.ToString()))
                            sizey = new FileInfo(((ListViewItem)y).Tag.ToString()).Length;
                        else {
                            if (col < 3)
                                sizey = new DriveInfo(((ListViewItem)y).Tag.ToString()).TotalSize;
                            else sizey = new DriveInfo(((ListViewItem)y).Tag.ToString()).TotalFreeSpace;
                        }
                    }
                    catch {
                        sizey = 0;
                    }
                    if (sort_b)
                        return sizex - sizey > 0 ? 1 : sizex - sizey == 0 ? 0 : -1;
                    else return sizey - sizex > 0 ? 1 : sizey - sizex == 0 ? 0 : -1;
                } else if (columName.Contains("时间")) {
                    DateTime xx = Convert.ToDateTime(((ListViewItem)x).SubItems[col].Text);
                    DateTime yy = Convert.ToDateTime(((ListViewItem)y).SubItems[col].Text);
                    if (sort_b)
                        return DateTime.Compare(xx, yy);
                    else return DateTime.Compare(yy, xx);
                } else {

                    if (sort_b) {
                        return String.Compare(((ListViewItem)x).SubItems[col].Text, ((ListViewItem)y).SubItems[col].Text);
                    } else {
                        return String.Compare(((ListViewItem)y).SubItems[col].Text, ((ListViewItem)x).SubItems[col].Text);
                    }
                }
            }
        }

    }
}
