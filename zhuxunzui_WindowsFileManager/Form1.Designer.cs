﻿namespace zhuxunzui_WindowsFileManager {
    partial class Form1 {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.ListViewGroup listViewGroup4 = new System.Windows.Forms.ListViewGroup("硬盘", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup5 = new System.Windows.Forms.ListViewGroup("移动存储", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup6 = new System.Windows.Forms.ListViewGroup("其他", System.Windows.Forms.HorizontalAlignment.Left);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.combo_url = new System.Windows.Forms.ComboBox();
            this.pbtn_next = new System.Windows.Forms.PictureBox();
            this.pbtn_pre = new System.Windows.Forms.PictureBox();
            this.fileSearch_btn = new System.Windows.Forms.PictureBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.复制ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.剪切ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.粘贴ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.重命名ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.属性ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.listViewDisplayType_btn = new System.Windows.Forms.ToolStripSplitButton();
            this.大图标ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.小图标ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.列表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.详细列表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.平铺ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reflush_btn = new System.Windows.Forms.ToolStripLabel();
            this.parent_node_btn = new System.Windows.Forms.ToolStripLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lb_objectNum = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.listView1 = new System.Windows.Forms.ListView();
            this.ch_name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ch_type = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ch_totle = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ch_avail = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.mouseRightMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsm_open = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_copy = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_pause = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_cut = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_del = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_rename = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_new = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_new_folder = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_new_txt = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_new_doc = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_new_exc = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_new_ppt = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_reflush = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_info = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_return = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_empty = new System.Windows.Forms.ToolStripMenuItem();
            this.treeView1 = new System.Windows.Forms.TreeView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbtn_next)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbtn_pre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSearch_btn)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.mouseRightMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.SteelBlue;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.combo_url);
            this.splitContainer1.Panel1.Controls.Add(this.pbtn_next);
            this.splitContainer1.Panel1.Controls.Add(this.pbtn_pre);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.fileSearch_btn);
            this.splitContainer1.Panel2.Controls.Add(this.textBox1);
            this.splitContainer1.Size = new System.Drawing.Size(1401, 50);
            this.splitContainer1.SplitterDistance = 1117;
            this.splitContainer1.TabIndex = 0;
            this.splitContainer1.SizeChanged += new System.EventHandler(this.splitContainer1_SizeChanged);
            // 
            // combo_url
            // 
            this.combo_url.Font = new System.Drawing.Font("宋体", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.combo_url.FormattingEnabled = true;
            this.combo_url.Location = new System.Drawing.Point(105, 12);
            this.combo_url.Name = "combo_url";
            this.combo_url.Size = new System.Drawing.Size(980, 29);
            this.combo_url.TabIndex = 2;
            // 
            // pbtn_next
            // 
            this.pbtn_next.BackColor = System.Drawing.Color.SteelBlue;
            this.pbtn_next.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbtn_next.BackgroundImage")));
            this.pbtn_next.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbtn_next.Location = new System.Drawing.Point(63, 12);
            this.pbtn_next.Name = "pbtn_next";
            this.pbtn_next.Size = new System.Drawing.Size(36, 26);
            this.pbtn_next.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbtn_next.TabIndex = 1;
            this.pbtn_next.TabStop = false;
            // 
            // pbtn_pre
            // 
            this.pbtn_pre.BackColor = System.Drawing.Color.SteelBlue;
            this.pbtn_pre.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbtn_pre.BackgroundImage")));
            this.pbtn_pre.Image = ((System.Drawing.Image)(resources.GetObject("pbtn_pre.Image")));
            this.pbtn_pre.Location = new System.Drawing.Point(12, 12);
            this.pbtn_pre.Name = "pbtn_pre";
            this.pbtn_pre.Size = new System.Drawing.Size(36, 26);
            this.pbtn_pre.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbtn_pre.TabIndex = 0;
            this.pbtn_pre.TabStop = false;
            // 
            // fileSearch_btn
            // 
            this.fileSearch_btn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.fileSearch_btn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("fileSearch_btn.BackgroundImage")));
            this.fileSearch_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fileSearch_btn.Location = new System.Drawing.Point(204, 12);
            this.fileSearch_btn.Name = "fileSearch_btn";
            this.fileSearch_btn.Size = new System.Drawing.Size(36, 26);
            this.fileSearch_btn.TabIndex = 2;
            this.fileSearch_btn.TabStop = false;
            this.fileSearch_btn.Click += new System.EventHandler(this.fileSearch_btn_Click);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox1.Location = new System.Drawing.Point(32, 10);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(157, 31);
            this.textBox1.TabIndex = 0;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSplitButton1,
            this.toolStripButton1,
            this.listViewDisplayType_btn,
            this.reflush_btn,
            this.parent_node_btn});
            this.toolStrip1.Location = new System.Drawing.Point(0, 50);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1401, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSplitButton1
            // 
            this.toolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripSplitButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.复制ToolStripMenuItem,
            this.剪切ToolStripMenuItem,
            this.粘贴ToolStripMenuItem,
            this.删除ToolStripMenuItem,
            this.重命名ToolStripMenuItem,
            this.属性ToolStripMenuItem});
            this.toolStripSplitButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton1.Image")));
            this.toolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton1.Name = "toolStripSplitButton1";
            this.toolStripSplitButton1.Size = new System.Drawing.Size(49, 22);
            this.toolStripSplitButton1.Text = "组织";
            // 
            // 复制ToolStripMenuItem
            // 
            this.复制ToolStripMenuItem.Name = "复制ToolStripMenuItem";
            this.复制ToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.复制ToolStripMenuItem.Text = "复制";
            // 
            // 剪切ToolStripMenuItem
            // 
            this.剪切ToolStripMenuItem.Name = "剪切ToolStripMenuItem";
            this.剪切ToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.剪切ToolStripMenuItem.Text = "剪切";
            // 
            // 粘贴ToolStripMenuItem
            // 
            this.粘贴ToolStripMenuItem.Name = "粘贴ToolStripMenuItem";
            this.粘贴ToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.粘贴ToolStripMenuItem.Text = "粘贴";
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.删除ToolStripMenuItem.Text = "删除";
            // 
            // 重命名ToolStripMenuItem
            // 
            this.重命名ToolStripMenuItem.Name = "重命名ToolStripMenuItem";
            this.重命名ToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.重命名ToolStripMenuItem.Text = "重命名";
            // 
            // 属性ToolStripMenuItem
            // 
            this.属性ToolStripMenuItem.Name = "属性ToolStripMenuItem";
            this.属性ToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.属性ToolStripMenuItem.Text = "属性";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(76, 22);
            this.toolStripButton1.Text = "新建文件夹";
            // 
            // listViewDisplayType_btn
            // 
            this.listViewDisplayType_btn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.listViewDisplayType_btn.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.大图标ToolStripMenuItem,
            this.小图标ToolStripMenuItem,
            this.列表ToolStripMenuItem,
            this.详细列表ToolStripMenuItem,
            this.平铺ToolStripMenuItem});
            this.listViewDisplayType_btn.Image = ((System.Drawing.Image)(resources.GetObject("listViewDisplayType_btn.Image")));
            this.listViewDisplayType_btn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.listViewDisplayType_btn.Name = "listViewDisplayType_btn";
            this.listViewDisplayType_btn.Size = new System.Drawing.Size(32, 22);
            this.listViewDisplayType_btn.Text = "listViewDisplayType_btn";
            this.listViewDisplayType_btn.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.ToolStripMenuItem_Click);
            // 
            // 大图标ToolStripMenuItem
            // 
            this.大图标ToolStripMenuItem.Name = "大图标ToolStripMenuItem";
            this.大图标ToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.大图标ToolStripMenuItem.Text = "大图标";
            // 
            // 小图标ToolStripMenuItem
            // 
            this.小图标ToolStripMenuItem.Name = "小图标ToolStripMenuItem";
            this.小图标ToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.小图标ToolStripMenuItem.Text = "小图标";
            // 
            // 列表ToolStripMenuItem
            // 
            this.列表ToolStripMenuItem.Name = "列表ToolStripMenuItem";
            this.列表ToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.列表ToolStripMenuItem.Text = "列表";
            // 
            // 详细列表ToolStripMenuItem
            // 
            this.详细列表ToolStripMenuItem.Checked = true;
            this.详细列表ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.详细列表ToolStripMenuItem.Name = "详细列表ToolStripMenuItem";
            this.详细列表ToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.详细列表ToolStripMenuItem.Text = "详细列表";
            // 
            // 平铺ToolStripMenuItem
            // 
            this.平铺ToolStripMenuItem.Name = "平铺ToolStripMenuItem";
            this.平铺ToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.平铺ToolStripMenuItem.Text = "平铺";
            // 
            // reflush_btn
            // 
            this.reflush_btn.Name = "reflush_btn";
            this.reflush_btn.Size = new System.Drawing.Size(33, 22);
            this.reflush_btn.Text = "刷新";
            // 
            // parent_node_btn
            // 
            this.parent_node_btn.Name = "parent_node_btn";
            this.parent_node_btn.Size = new System.Drawing.Size(59, 22);
            this.parent_node_btn.Text = "返回上级";
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.lb_objectNum,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 612);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1401, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(59, 17);
            this.toolStripStatusLabel1.Text = "项目数：";
            // 
            // lb_objectNum
            // 
            this.lb_objectNum.Name = "lb_objectNum";
            this.lb_objectNum.Size = new System.Drawing.Size(13, 17);
            this.lb_objectNum.Text = "0";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(59, 17);
            this.toolStripStatusLabel2.Text = "正在搜索";
            this.toolStripStatusLabel2.Visible = false;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 75);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.BackColor = System.Drawing.Color.White;
            this.splitContainer2.Panel1.Controls.Add(this.treeView1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.BackColor = System.Drawing.Color.White;
            this.splitContainer2.Panel2.Controls.Add(this.listView1);
            this.splitContainer2.Panel2.SizeChanged += new System.EventHandler(this.splitContainer2_Panel2_SizeChanged);
            this.splitContainer2.Size = new System.Drawing.Size(1401, 537);
            this.splitContainer2.SplitterDistance = 224;
            this.splitContainer2.TabIndex = 3;
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // listView1
            // 
            this.listView1.AllowColumnReorder = true;
            this.listView1.BackColor = System.Drawing.Color.White;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ch_name,
            this.ch_type,
            this.ch_totle,
            this.ch_avail});
            this.listView1.FullRowSelect = true;
            listViewGroup4.Header = "硬盘";
            listViewGroup4.Name = "lv_group1";
            listViewGroup5.Header = "移动存储";
            listViewGroup5.Name = "lv_group2";
            listViewGroup6.Header = "其他";
            listViewGroup6.Name = "lv_group3";
            this.listView1.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup4,
            listViewGroup5,
            listViewGroup6});
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1170, 534);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listView1_ColumnClick);
            this.listView1.MouseEnter += new System.EventHandler(this.listView1_MouseEnter);
            // 
            // ch_name
            // 
            this.ch_name.Text = "名称";
            // 
            // ch_type
            // 
            this.ch_type.Text = "类型";
            // 
            // ch_totle
            // 
            this.ch_totle.Text = "大小";
            // 
            // ch_avail
            // 
            this.ch_avail.Text = "可用空间";
            // 
            // imageList2
            // 
            this.imageList2.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList2.ImageSize = new System.Drawing.Size(32, 32);
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // mouseRightMenu
            // 
            this.mouseRightMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_open,
            this.tsm_copy,
            this.tsm_pause,
            this.tsm_cut,
            this.tsm_del,
            this.tsm_rename,
            this.tsm_new,
            this.tsm_reflush,
            this.tsm_info,
            this.tsm_return,
            this.tsm_empty});
            this.mouseRightMenu.Name = "mouseRightMenu";
            this.mouseRightMenu.Size = new System.Drawing.Size(114, 246);
            // 
            // tsm_open
            // 
            this.tsm_open.Name = "tsm_open";
            this.tsm_open.Size = new System.Drawing.Size(113, 22);
            this.tsm_open.Tag = "10";
            this.tsm_open.Text = "打开";
            // 
            // tsm_copy
            // 
            this.tsm_copy.Name = "tsm_copy";
            this.tsm_copy.Size = new System.Drawing.Size(113, 22);
            this.tsm_copy.Tag = "8";
            this.tsm_copy.Text = "复制";
            // 
            // tsm_pause
            // 
            this.tsm_pause.Name = "tsm_pause";
            this.tsm_pause.Size = new System.Drawing.Size(113, 22);
            this.tsm_pause.Tag = "4";
            this.tsm_pause.Text = "粘贴";
            // 
            // tsm_cut
            // 
            this.tsm_cut.Name = "tsm_cut";
            this.tsm_cut.Size = new System.Drawing.Size(113, 22);
            this.tsm_cut.Tag = "8";
            this.tsm_cut.Text = "剪切";
            // 
            // tsm_del
            // 
            this.tsm_del.Name = "tsm_del";
            this.tsm_del.Size = new System.Drawing.Size(113, 22);
            this.tsm_del.Tag = "40";
            this.tsm_del.Text = "删除";
            // 
            // tsm_rename
            // 
            this.tsm_rename.Name = "tsm_rename";
            this.tsm_rename.Size = new System.Drawing.Size(113, 22);
            this.tsm_rename.Tag = "10";
            this.tsm_rename.Text = "重命名";
            // 
            // tsm_new
            // 
            this.tsm_new.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_new_folder,
            this.tsm_new_txt,
            this.tsm_new_doc,
            this.tsm_new_exc,
            this.tsm_new_ppt});
            this.tsm_new.Name = "tsm_new";
            this.tsm_new.Size = new System.Drawing.Size(113, 22);
            this.tsm_new.Tag = "4";
            this.tsm_new.Text = "新建";
            // 
            // tsm_new_folder
            // 
            this.tsm_new_folder.Image = ((System.Drawing.Image)(resources.GetObject("tsm_new_folder.Image")));
            this.tsm_new_folder.Name = "tsm_new_folder";
            this.tsm_new_folder.Size = new System.Drawing.Size(147, 22);
            this.tsm_new_folder.Text = "文件夹";
            // 
            // tsm_new_txt
            // 
            this.tsm_new_txt.Image = ((System.Drawing.Image)(resources.GetObject("tsm_new_txt.Image")));
            this.tsm_new_txt.Name = "tsm_new_txt";
            this.tsm_new_txt.Size = new System.Drawing.Size(147, 22);
            this.tsm_new_txt.Text = "文本文档";
            // 
            // tsm_new_doc
            // 
            this.tsm_new_doc.Image = ((System.Drawing.Image)(resources.GetObject("tsm_new_doc.Image")));
            this.tsm_new_doc.Name = "tsm_new_doc";
            this.tsm_new_doc.Size = new System.Drawing.Size(147, 22);
            this.tsm_new_doc.Text = "WORD文档";
            // 
            // tsm_new_exc
            // 
            this.tsm_new_exc.Image = ((System.Drawing.Image)(resources.GetObject("tsm_new_exc.Image")));
            this.tsm_new_exc.Name = "tsm_new_exc";
            this.tsm_new_exc.Size = new System.Drawing.Size(147, 22);
            this.tsm_new_exc.Text = "EXECEL文档";
            // 
            // tsm_new_ppt
            // 
            this.tsm_new_ppt.Image = ((System.Drawing.Image)(resources.GetObject("tsm_new_ppt.Image")));
            this.tsm_new_ppt.Name = "tsm_new_ppt";
            this.tsm_new_ppt.Size = new System.Drawing.Size(147, 22);
            this.tsm_new_ppt.Text = "PPT演示文档";
            // 
            // tsm_reflush
            // 
            this.tsm_reflush.Name = "tsm_reflush";
            this.tsm_reflush.Size = new System.Drawing.Size(113, 22);
            this.tsm_reflush.Tag = "21";
            this.tsm_reflush.Text = "刷新";
            // 
            // tsm_info
            // 
            this.tsm_info.Name = "tsm_info";
            this.tsm_info.Size = new System.Drawing.Size(113, 22);
            this.tsm_info.Tag = "10";
            this.tsm_info.Text = "属性";
            // 
            // tsm_return
            // 
            this.tsm_return.Name = "tsm_return";
            this.tsm_return.Size = new System.Drawing.Size(113, 22);
            this.tsm_return.Tag = "32";
            this.tsm_return.Text = "还原";
            // 
            // tsm_empty
            // 
            this.tsm_empty.Name = "tsm_empty";
            this.tsm_empty.Size = new System.Drawing.Size(113, 22);
            this.tsm_empty.Tag = "16";
            this.tsm_empty.Text = "清空";
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(224, 537);
            this.treeView1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1401, 634);
            this.Controls.Add(this.splitContainer2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form1";
            this.Text = "祝训醉文件管理器";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbtn_next)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbtn_pre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSearch_btn)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.mouseRightMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ComboBox combo_url;
        private System.Windows.Forms.PictureBox pbtn_next;
        private System.Windows.Forms.PictureBox pbtn_pre;
        private System.Windows.Forms.PictureBox fileSearch_btn;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;
        private System.Windows.Forms.ToolStripMenuItem 复制ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 剪切ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 粘贴ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 重命名ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 属性ToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSplitButton listViewDisplayType_btn;
        private System.Windows.Forms.ToolStripMenuItem 大图标ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 小图标ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 列表ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 详细列表ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 平铺ToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader ch_name;
        private System.Windows.Forms.ColumnHeader ch_type;
        private System.Windows.Forms.ColumnHeader ch_totle;
        private System.Windows.Forms.ColumnHeader ch_avail;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lb_objectNum;
        private System.Windows.Forms.ToolStripLabel reflush_btn;
        private System.Windows.Forms.ToolStripLabel parent_node_btn;
        private System.Windows.Forms.ContextMenuStrip mouseRightMenu;
        private System.Windows.Forms.ToolStripMenuItem tsm_copy;
        private System.Windows.Forms.ToolStripMenuItem tsm_pause;
        private System.Windows.Forms.ToolStripMenuItem tsm_cut;
        private System.Windows.Forms.ToolStripMenuItem tsm_del;
        private System.Windows.Forms.ToolStripMenuItem tsm_rename;
        private System.Windows.Forms.ToolStripMenuItem tsm_new;
        private System.Windows.Forms.ToolStripMenuItem tsm_reflush;
        private System.Windows.Forms.ToolStripMenuItem tsm_info;
        private System.Windows.Forms.ToolStripMenuItem tsm_open;
        private System.Windows.Forms.ToolStripMenuItem tsm_return;
        private System.Windows.Forms.ToolStripMenuItem tsm_empty;
        private System.Windows.Forms.ToolStripMenuItem tsm_new_folder;
        private System.Windows.Forms.ToolStripMenuItem tsm_new_txt;
        private System.Windows.Forms.ToolStripMenuItem tsm_new_doc;
        private System.Windows.Forms.ToolStripMenuItem tsm_new_exc;
        private System.Windows.Forms.ToolStripMenuItem tsm_new_ppt;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.TreeView treeView1;

    }
}

