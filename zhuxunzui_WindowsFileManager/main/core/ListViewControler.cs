﻿//using Microsoft.Analytics.Interfaces;
//using Microsoft.Analytics.Types.Sql;
using JHR_GetIcon;
using Microsoft.WindowsAPICodePack.Shell;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using Shell32;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using zhuxunzui_WindowsFileManager.main.controler;
using zhuxunzui_WindowsFileManager.main.util;
//using Winshell;

namespace zhuxunzui_WindowsFileManager.main.core
{
    class ListViewControler
    {
        private static ListView listView;

        public static void setListView(ListView lv)
        {
            listView = lv;
        }

        private static void CreateCol_F()
        {
            listView.Columns.Clear();


            listViewAddColumsItem("名称", "chname");
            listViewAddColumsItem("类型", "chtype");
            listViewAddColumsItem("修改时间", "chmodify");
            listViewAddColumsItem("大小", "chtotal");
            listViewAddColumsItem("创建时间", "chcreate");


        }

        private static void CreateCol_D()
        {
            //为listview创建列(名称，大小，总大小，可用大小)
            listView.Columns.Clear();

            listViewAddColumsItem("名称", "chname");
            listViewAddColumsItem("类型", "chtype");
            listViewAddColumsItem("总大小", "chtotal");
            listViewAddColumsItem("可用大小", "chfree");

        }


        public static void init()
        {

            listView.Items.Clear();
            listView.LargeImageList = IconHelper.getImage_B();
            listView.StateImageList = IconHelper.getImage_S();
            listView.SmallImageList = IconHelper.getImage_S();
            getDriverListView();
            listView.ItemActivate += new EventHandler(listViewItemActivate);

        }

        public static void getDriverListView()
        {
            listView.Items.Clear();
            CreateCol_D();//创建列
            //CreateCol_F();//创建另一种列？
            
            DriveInfo[] drivers = DriveInfo.GetDrives();
            string lvname1, lvname2, lvtype, keyname, lvtotal = "", lvfree = "";
            foreach (DriveInfo driver in drivers)
            {
                ListViewItem newitem = new ListViewItem();
                newitem.IndentCount = 1;
                if (driver.IsReady) lvname1 = driver.VolumeLabel;//卷标，没有则为空串
                else lvname1 = "";
                lvname2 = driver.Name;//驱动器的名称
                switch (driver.DriveType)
                {
                    case DriveType.Fixed:
                        {//本地磁盘
                            keyname = Const.LOCALDRIVER;
                            lvtype = "本地磁盘";
                            if (lvname1.Equals("")) lvname1 = "本地磁盘";
                            newitem.Group = listView.Groups["lv_group1"];
                            break;
                        }
                    case DriveType.Removable:
                        {//移动存储
                            keyname = Const.MOVABLEDRIVER;
                            lvtype = "移动存储";
                            if (lvname1.Equals("")) lvname1 = "移动存储";
                            newitem.Group = listView.Groups["lv_group2"];
                            break;
                        }
                    case DriveType.CDRom:
                        {//光驱
                            keyname = Const.CDROM;
                            lvtype = "光盘驱动器";
                            if (lvname1.Equals("")) lvname1 = "光盘启动器";
                            newitem.Group = listView.Groups["lv_group2"];
                            break;
                        }
                    default:
                        {//未知驱动器
                            keyname = Const.UNKNOWICON;
                            lvtype = "未知设备";
                            if (lvname1.Equals("")) lvname1 = "未知设备";
                            newitem.Group = listView.Groups["lv_group3"];
                            break;
                        }
                }
                newitem.SubItems[0].Text = (lvname1 + "(" + lvname2.Substring(0, 2) + ")");//名称
                newitem.SubItems.Add(lvtype);//类型
                if (driver.IsReady)
                {
                    lvtotal = Math.Round(driver.TotalSize / (1024 * 1024 * 1024 * 1.0), 1).ToString() + "G";
                    lvfree = Math.Round(driver.TotalFreeSpace / (1024 * 1024 * 1024 * 1.0), 1).ToString() + "G";
                }
                newitem.SubItems.Add(lvtotal);//总大小
                newitem.SubItems.Add(lvfree);//可用的空间大小
                newitem.ImageKey = keyname;//图标的key
                newitem.Tag = lvname2;//保存路径
                listView.Items.Add(newitem);
            }
            StatusToolBarHelper.setObjectNum(listView.Items.Count.ToString());
        }

        public static int getFolderListView(String p)
        {

            listView.Items.Clear();
            CreateCol_F();
            string[] dirs;
            string[] files;
            try
            {
                dirs = Directory.GetDirectories(p);//获取路径p的子目录
                files = Directory.GetFiles(p);//获取路径p下的文件
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
                return 1;
            }
            //************************************************************************************
            foreach (string dir in dirs)//处理目录对象
            {
                try
                {
                    DirectoryInfo dinfo = new DirectoryInfo(dir);
                    ListViewItem lv = new ListViewItem(dinfo.Name);
                    lv.Tag = dinfo.FullName;
                    lv.ImageKey = Const.DEFAULTFOLDER;
                    lv.SubItems.Add("文件夹");//类型
                    lv.SubItems.Add(dinfo.LastWriteTime.ToString());//修改时间

                    //lv.SubItems.Add(FolderHelPer.getSizeString(dinfo.FullName)); //获取大小
                    lv.SubItems.Add("--"); //获取大小 会导致卡死 还是不要用这个功能了
                    lv.SubItems.Add(dinfo.CreationTime.ToString());//创建时间
                    listView.Items.Add(lv);
                }
                catch { }
            }

            foreach (string f in files)//读取文件
            {
                try
                {
                    FileInfo finfo = new FileInfo(f);
                    ListViewItem lv = new ListViewItem(finfo.Name);//名称
                    lv.Tag = finfo.FullName;
                    lv.ImageKey = GetFileIconKey(finfo.Extension, finfo.FullName);//根据扩展名提取图标
                    string typename = new GetIcon().GetTypeName(finfo.FullName);//获取文件类型名称
                    lv.SubItems.Add(typename);//类型
                    lv.SubItems.Add(finfo.LastWriteTime.ToString());//修改时间
                    lv.SubItems.Add(FolderHelPer.changeFileSizeFormatToString(finfo.Length));//大小
                    lv.SubItems.Add(finfo.CreationTime.ToString());//创建时间
                    listView.Items.Add(lv);
                }
                catch { }
            }
            StatusToolBarHelper.setObjectNum(listView.Items.Count.ToString());
            //lb_ojbnum.Text = listView.Items.Count.ToString();
            return 0;

            //return 0;
        }

        public static void getFavoritesListView()
        {
            listView.Items.Clear();
            CreateCol_F();
            string mypath = "";

            mypath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            ListViewItem lv = new ListViewItem("我的文档");
            lv.Tag = mypath;
            lv.ImageKey = Const.MYDOCUMENT;
            DirectoryInfo dinfo = new DirectoryInfo(mypath);
            lv.SubItems.Add("文件夹");//类型
            lv.SubItems.Add(dinfo.LastWriteTime.ToString());//修改时间
            lv.SubItems.Add("");
            lv.SubItems.Add(dinfo.CreationTime.ToString());//创建时间
            listView.Items.Add(lv);

            mypath = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);
            if (mypath != null && !mypath.Equals(""))
            {
                lv = new ListViewItem("我的音乐");
                lv.Tag = mypath;
                lv.ImageKey = Const.MYMUSIC;
                dinfo = new DirectoryInfo(mypath);
                lv.SubItems.Add("文件夹");//类型
                lv.SubItems.Add(dinfo.LastWriteTime.ToString());//修改时间
                lv.SubItems.Add("");//大小
                lv.SubItems.Add(dinfo.CreationTime.ToString());//创建时间
                listView.Items.Add(lv);

            }

            mypath = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            if (mypath != null && !mypath.Equals(""))
            {
                lv = new ListViewItem("我的图片");
                lv.Tag = mypath;
                lv.ImageKey = Const.MYPICTURES;
                dinfo = new DirectoryInfo(mypath);
                lv.SubItems.Add("文件夹");//类型
                lv.SubItems.Add(dinfo.LastWriteTime.ToString());//修改时间
                lv.SubItems.Add("");//大小
                lv.SubItems.Add(dinfo.CreationTime.ToString());//创建时间
                listView.Items.Add(lv);

            }

            mypath = Environment.GetFolderPath(Environment.SpecialFolder.MyVideos);
            if (mypath != null && !mypath.Equals(""))
            {
                lv = new ListViewItem("我的视频");
                lv.Tag = mypath;
                lv.ImageKey = Const.MYVIDEOS;
                dinfo = new DirectoryInfo(mypath);
                lv.SubItems.Add("文件夹");//类型
                lv.SubItems.Add(dinfo.LastWriteTime.ToString());//修改时间
                lv.SubItems.Add("");//大小
                lv.SubItems.Add(dinfo.CreationTime.ToString());//创建时间
                listView.Items.Add(lv);

            }
            StatusToolBarHelper.setObjectNum(listView.Items.Count.ToString());
            //lb_ojbnum.Text = listView.Items.Count.ToString();
        }



        public static void doSearchFile(string topfolder, string content) {
            if (Directory.Exists(topfolder)) {
                try {
                    string[] files = Directory.GetFiles(topfolder);
                    foreach (string f in files) {
                        try {
                            FileInfo finfo = new FileInfo(f);
                            if (finfo.Name.ToUpper().IndexOf(content.ToUpper()) != -1)//如果文件包含搜索内容，则获取文件信息显示
                            {
                                ListViewItem lv = new ListViewItem(finfo.Name);//名称
                                lv.Tag = finfo.FullName;
                                lv.IndentCount = 1;
                                lv.ImageKey = GetFileIconKey(finfo.Extension, finfo.FullName);//根据扩展名提取图标
                                string typename = new GetIcon().GetTypeName(finfo.FullName);//获取文件类型名称
                                lv.SubItems.Add(typename);//类型
                                lv.SubItems.Add(finfo.FullName);//位置
                                lv.SubItems.Add(finfo.LastWriteTime.ToString());//修改时间
                                listView.Items.Add(lv);

                            }
                        }
                        catch { }
                    }
                    string[] dirs = Directory.GetDirectories(topfolder);
                    foreach (string d in dirs) doSearchFile(d, content);
                }
                catch { }
            }
        }

        public static void getRecyleListView()
        {
            listView.Items.Clear();
            CreateCol_R();
            Shell shell = new Shell();
            Folder recycleBin = shell.NameSpace(10);
            // recycleBin.f
            string recyclePath = "";
            //Folder recycleBin.ParentFolder
            foreach (FolderItem f in recycleBin.Items())
            {
                ListViewItem lv = new ListViewItem(f.Name);
                string [] ss = {"",""};
               // lv.Tag = ss;
                ss[0] = f.Path;
                recyclePath = f.Path;

                ShellPropertyCollection cooll = new ShellPropertyCollection(recyclePath);
                var properties = cooll.Where(prop => prop.CanonicalName != null).OrderBy(prop => prop.CanonicalName).ToArray();
                Array.ForEach(
                   properties,
                   p =>
                   {
                       //DisplayPropertyValue(p);
                       if(p.CanonicalName.Equals("System.Recycle.DeletedFrom"))
                           ss[1] = (string)p.ValueAsObject;
                   });

                lv.Tag = ss;



                if (f.Name.LastIndexOf('.') > -1)
                {
                    lv.ImageKey = GetFileIconKey(f.Name.Substring(f.Name.LastIndexOf('.')), f.Path);
                }
                else
                {
                    if (f.IsFolder) lv.ImageKey = Const.DEFAULTFOLDER;
                    else lv.ImageKey = Const.UNKNOWICON;
                }
                lv.SubItems.Add(f.Type);
                lv.SubItems.Add(((string [])lv.Tag)[1]);
                lv.SubItems.Add(f.ModifyDate.ToString());
                listView.Items.Add(lv);

            }
            //recyclePath = recyclePath.Substring(0, recyclePath.LastIndexOf("\\"));
            //Console.WriteLine(recyclePath);



            
            
            StatusToolBarHelper.setObjectNum(listView.Items.Count.ToString());
        }

        //获取文件信息
        private static void DisplayPropertyValue(IShellProperty prop)
        {

            string value = string.Empty;
            value = prop.ValueAsObject == null ? "null" : prop.FormatForDisplay(
                    PropertyDescriptionFormatOptions.None);
            Console.WriteLine(prop.CanonicalName + "-------" + value);
        }


        public static void CreateCol_R()
        {
            listView.Columns.Clear();
            listViewAddColumsItem("名称", "chname");
            listViewAddColumsItem("类型", "chtype");
            listViewAddColumsItem("位置", "chpath");
            listViewAddColumsItem("修改日期", "chdel");
        }

        private static void listViewAddColumsItem(string text, string name)
        {
            ColumnHeader c = new ColumnHeader();
            c.Text = text;
            c.TextAlign = HorizontalAlignment.Left;
            c.Width = 200;
            c.Name = name;
            listView.Columns.Add(c);
        }

        //读取桌面的对象，显示在listView中
        //分两步进行读取
        //1）读取我的电脑，回收站，我的文档，网络，这些特殊对象
        //2）然后再读取桌面的普通的文件夹和文件对象
        public static void getDesktopListView()
        {
            //1）特殊对象的读取
            listView.Items.Clear();
            CreateCol_F();

            ListViewItem lv = new ListViewItem("我的电脑");
            lv.Tag = Const.MYCOMPUTERPATH;
            lv.ImageKey = Const.COMPUTER;
            lv.SubItems.Add("");
            lv.SubItems.Add("");
            lv.SubItems.Add("");
            lv.SubItems.Add("");
            listView.Items.Add(lv);

            lv = new ListViewItem("我的文档");
            lv.Tag = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            lv.ImageKey = Const.MYDOCUMENT;
            lv.SubItems.Add("");
            lv.SubItems.Add("");
            lv.SubItems.Add("");
            lv.SubItems.Add("");
            listView.Items.Add(lv);

            lv = new ListViewItem("网络");
            lv.Tag = Const.NETWORK;
            lv.ImageKey = Const.NETWORK;
            lv.SubItems.Add("");
            lv.SubItems.Add("");
            lv.SubItems.Add("");
            lv.SubItems.Add("");
            listView.Items.Add(lv);

            lv = new ListViewItem("回收站");
            lv.Tag = Const.RECYCLE;
            lv.ImageKey = Const.RECYCLE;
            lv.SubItems.Add("");
            lv.SubItems.Add("");
            lv.SubItems.Add("");
            lv.SubItems.Add("");
            listView.Items.Add(lv);




            //2）普通文件和文件夹对象的读取
            //获取桌面目录和文件
            string[] dirs;
            string[] files;
            string p = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            try
            {
                dirs = Directory.GetDirectories(p);
                files = Directory.GetFiles(p);
                foreach (string dir in dirs)
                {
                    try
                    {
                        DirectoryInfo dinfo = new DirectoryInfo(dir);
                        lv = new ListViewItem(dinfo.Name);
                        lv.Tag = dinfo.FullName;
                        lv.ImageKey = Const.DEFAULTFOLDER;
                        lv.SubItems.Add("文件夹");//类型
                        lv.SubItems.Add(dinfo.LastWriteTime.ToString());//修改时间
                        lv.SubItems.Add("大小");
                        lv.SubItems.Add(dinfo.CreationTime.ToString());//创建时间
                        listView.Items.Add(lv);
                    }
                    catch { }
                }
                foreach (string f in files)//读取文件
                {
                    try
                    {
                        FileInfo finfo = new FileInfo(f);
                        lv = new ListViewItem(finfo.Name);//名称
                        lv.Tag = finfo.FullName;
                        lv.ImageKey = GetFileIconKey(finfo.Extension, finfo.FullName);//根据扩展名提取图标
                        string typename = new GetIcon().GetTypeName(finfo.FullName);//获取文件类型名称
                        lv.SubItems.Add(typename);//类型
                        lv.SubItems.Add(finfo.LastWriteTime.ToString());//修改时间
                        long size = finfo.Length;
                        string sizestring = "";
                        if (size < 1024) sizestring = size.ToString() + "Byte";
                        else sizestring = (size / 1024).ToString() + "KB";
                        lv.SubItems.Add(sizestring);//大小
                        lv.SubItems.Add(finfo.CreationTime.ToString());//创建时间
                        listView.Items.Add(lv);
                    }
                    catch { }
                }

            }
            catch { }

            //lb_ojbnum.Text = listView.Items.Count.ToString();
            StatusToolBarHelper.setObjectNum(listView.Items.Count.ToString());
        }


        public static string GetFileIconKey(string exten, string fullname)
        {
            ////根据文件扩展名获取对应图标再imagelist数组中的key
            ////返回的是扩展名对应的图标的键的字符串

            string imgkey = "";
            Icon[] myIcon;
            //提取可执行文件/快捷方式的专用图标，如果失败则使用默认可执行文件图标/未知文件图标
            if (exten.ToUpper().Equals(".EXE") || exten.ToUpper().Equals(".LNK"))
            {
                myIcon = new GetIcon().GetIconByFileName(fullname, FileAttributes.Normal);
                if (myIcon != null)
                {
                    if (myIcon[0] != null && myIcon[1] != null)
                    {
                        IconHelper.setIcon(fullname, myIcon);
                        imgkey = fullname;
                    }
                }

                if (imgkey == "")//如果获取图标失败，则设置默认图标
                {
                    if (exten.ToUpper().Equals(".EXE")) imgkey = Const.DEFAULTEXEICON;
                    else imgkey = Const.UNKNOWICON;
                }
            }
            else//根据扩展名提取图标，如果失败，则使用默认的未知文件图标
            {
                myIcon = new GetIcon().GetIconByFileType(exten);
                if (myIcon != null)
                {
                    if (myIcon[0] != null && myIcon[1] != null)
                    {
                        IconHelper.setIcon(fullname, myIcon);
                        imgkey = fullname;
                    }
                    else imgkey = Const.UNKNOWICON; ;
                }
                else imgkey = Const.UNKNOWICON; ;
            }
            return imgkey;
        }

        public static void updateListViewByPath(string path)
        {
            int flag = 0;
            switch (path)
            {
                case Const.DESKTOP:
                    {
                        //if (accesspaths.IndexOf("桌面") > -1) accesspaths.Remove("桌面");
                        //accesspaths.Insert(0, "桌面");
                        //AccessPathHistory.getAccessPathHistoryObject().addHistory(Const.DESKTOP);
                        ListViewControler.getDesktopListView();
                        break;
                    }
                case Const.MYCOMPUTERPATH:
                    {
                        //if (accesspaths.IndexOf("我的电脑") > -1) accesspaths.Remove("我的电脑");
                        //accesspaths.Insert(0, "我的电脑");
                        //AccessPathHistory.getAccessPathHistoryObject().addHistory(Const.MYCOMPUTERPATH);
                        ListViewControler.getDriverListView();
                        break;
                    }
                case Const.RECYCLE://回收站目前不提供还原功能
                    {
                        //if (accesspaths.IndexOf("回收站") > -1) accesspaths.Remove("回收站");
                        //accesspaths.Insert(0, "回收站");
                        //AccessPathHistory.getAccessPathHistoryObject().addHistory(Const.RECYCLE);
                        ListViewControler.getRecyleListView();
                        break;
                    }
                case Const.FAVORITES://回收站目前不提供还原功能
                    {
                        //if (accesspaths.IndexOf("收藏夹") > -1) accesspaths.Remove("收藏夹");
                        //accesspaths.Insert(0, "收藏夹");
                        //AccessPathHistory.getAccessPathHistoryObject().addHistory(Const.FAVORITES);
                        ListViewControler.getFavoritesListView();
                        break;
                    }
                default:
                    {
                        flag = ListViewControler.getFolderListView(path);
                        if (flag == 0)
                        {
                            //if (accesspaths.IndexOf(tn.Tag.ToString()) > -1) accesspaths.Remove(tn.Tag.ToString());
                            //accesspaths.Insert(0, tn.Tag.ToString());
                            //AccessPathHistory.getAccessPathHistoryObject().addHistory(path);
                        }
                        break;

                    }
            }



            if (flag == 0)
            {
                //AccessPathHistory.getAccessPathHistoryObject().addHistory(path);
                //Combo_urlControler.getCombo_urlControlerObject(null).updateItems(AccessPathHistory.getAccessPathHistoryObject().getAccressHistory());
            }
            else MessageBox.Show("访问失败，缺少权限或设备未就绪", "错误");

        }

        public static void listViewItemActivate(object sender, EventArgs e)
        {
            //Console.WriteLine("listview item double click");
            
            string currentPath = (string)AccessPathHistory.getAccessPathHistoryObject().getAccressHistory()[AccessPathHistory.getAccessPathHistoryObject().getCurrentPathIndex()];
            if (currentPath.Equals(Const.RECYCLE)) {

                zxz_attr_form z = new zxz_attr_form(((string [])((ListView)sender).FocusedItem.Tag)[1]);
                z.Show();
                return;
            }
            string path = ((ListView)sender).FocusedItem.Tag.ToString().Trim();
            if (File.Exists(path))
            {
                try
                {
                    System.Diagnostics.Process.Start(path);
                }
                catch
                {
                    MessageBox.Show("无法打开或运行该文件", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                return;
            }
            updateListViewByPath(path);
            AccessPathHistory.getAccessPathHistoryObject().addHistory(path);
            Combo_urlControler.getCombo_urlControlerObject(null).updateItems(AccessPathHistory.getAccessPathHistoryObject().getAccressHistory());
        }

        public static void reflushListView(object sender, EventArgs e)
        {
            updateListViewByPath((String)AccessPathHistory.getAccessPathHistoryObject().getAccressHistory()[AccessPathHistory.getAccessPathHistoryObject().getCurrentPathIndex()]);
        }

        public static void returnParentNode(object sender, EventArgs e)
        {
            string currentPath = (String)AccessPathHistory.getAccessPathHistoryObject().getAccressHistory()[AccessPathHistory.getAccessPathHistoryObject().getCurrentPathIndex()];
            string path = Const.MYCOMPUTERPATH;
            if (File.Exists(currentPath) || Directory.Exists(currentPath))
            {
                try
                {
                    path = Directory.GetParent(currentPath).FullName;
                }
                catch
                {
                    path = Const.MYCOMPUTERPATH;
                }
            }
            else
            {
                if (!currentPath.Equals(Const.DESKTOP))
                {
                    path = Const.DESKTOP;
                }
                else return;
            }
            updateListViewByPath(path);
            AccessPathHistory.getAccessPathHistoryObject().addHistory(path);
            Combo_urlControler.getCombo_urlControlerObject(null).updateItems(AccessPathHistory.getAccessPathHistoryObject().getAccressHistory());
        }

    }
}