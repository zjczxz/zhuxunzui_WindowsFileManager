﻿using JHR_GetIcon;
using System;
using System.IO;
using System.Windows.Forms;
using zhuxunzui_WindowsFileManager.main.controler;
using zhuxunzui_WindowsFileManager.main.util;
namespace zhuxunzui_WindowsFileManager.main.core
{
    class TreeNodeControler {
        private static TreeView treeView;

        public static void setTreeView(TreeView tv) { treeView = tv; }

        public static void init()
        {
            
            treeView.BeginUpdate();
            treeView.Nodes.Clear();
            treeView.ImageList = IconHelper.getImage_S();
            string path;
            treeViewAddNode(Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
                , Const.DESKTOP, Const.DESKTOP);

            path = Const.MYCOMPUTERPATH;
            //TreeNode root = new TreeNode("我的电脑");
            TreeNode root = new TreeNode(Const.MYCOMPUTERPATH);
            root.Text = Const.MYCOMPUTERPATH;
            root.SelectedImageKey = root.ImageKey = Const.COMPUTER;
            root.Tag = path;
            treeView.Nodes.Add(root);

            GetDriverTree(root);//在我的电脑节点下增加驱动器节点
            root.Expand();//展开我的电脑节点

            //收藏夹
            path = Const.FAVORITES;
            TreeNode tnf = new TreeNode(Const.FAVORITES);
            tnf.SelectedImageKey = tnf.ImageKey = Const.FAVORITES;
            tnf.Tag = path;
            treeView.Nodes.Add(tnf);

            //在收藏夹节点下添加：我的文档，我的图片，我的音乐，我的视频

            FavoritesAddNode(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), tnf, Const.MYDOCUMENT, "我的文档");
            FavoritesAddNode(Environment.GetFolderPath(Environment.SpecialFolder.MyMusic), tnf, Const.MYMUSIC, "我的音乐");
            FavoritesAddNode(Environment.GetFolderPath(Environment.SpecialFolder.MyPictures), tnf, Const.MYPICTURES, "我的图片");
            FavoritesAddNode(Environment.GetFolderPath(Environment.SpecialFolder.MyVideos), tnf, Const.MYVIDEOS, "我的视频");
            

            

            //treeViewAddNode(Const.RECYCLE, Const.RECYCLE, "回收站");
            treeViewAddNode(Const.RECYCLE, Const.RECYCLE, Const.RECYCLE);
            treeView.EndUpdate();
            //初始化listView
            //GetDriverListview();

            AccessPathHistory.getAccessPathHistoryObject().addHistory(Const.MYCOMPUTERPATH);

            Combo_urlControler.getCombo_urlControlerObject(null).updateItems(AccessPathHistory.getAccessPathHistoryObject().getAccressHistory());


            treeView.AfterSelect +=  new System.Windows.Forms.TreeViewEventHandler(AfterSelect);
            treeView.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(treeView_BeforeExpand);
        }

        private static void FavoritesAddNode(string path, TreeNode tnf, string key, string nodeName)
        {
            TreeNode tempNode = new TreeNode(nodeName);
            IconHelper.setIcon(key, new GetIcon().GetIconByFileName(path, FileAttributes.Directory));
            tempNode.SelectedImageKey = tempNode.ImageKey = key;
            tempNode.Tag = path;
            tnf.Nodes.Add(tempNode);
        }

        private static void treeViewAddNode(string path, string imageKey, string NodeName)
        {
            TreeNode tempNode = new TreeNode(NodeName);
            tempNode.ImageKey = tempNode.SelectedImageKey = imageKey;
            tempNode.Tag = NodeName;
            treeView.Nodes.Add(tempNode);
        }

        private static void GetDriverTree(TreeNode root)
        {
            DriveInfo[] drivers = DriveInfo.GetDrives();//获取驱动器集合
            string keyName = "";//驱动器类型键名
            string driverName = "";//驱动器卷标
            string driverTag = "";//驱动器实际名称  e.g. C:\\
            foreach (DriveInfo driver in drivers)
            {
                if (driver.IsReady) driverName = driver.VolumeLabel;//驱动器卷标
                else driverName = "";
                switch (driver.DriveType)
                {
                    case DriveType.Fixed:
                        keyName = Const.LOCALDRIVER;
                        if (driverName.Equals("")) driverName = "本地磁盘";
                        break;//本地磁盘
                    case DriveType.Removable:
                        keyName = Const.MOVABLEDRIVER;
                        if (driverName.Equals("")) driverName = "可移动磁盘";
                        break;//可移动磁盘
                    case DriveType.CDRom:
                        keyName = Const.CDROM;
                        if (driverName.Equals("")) driverName = "光盘驱动器";
                        break;
                    default:
                        keyName = Const.UNKNOWICON;
                        if (driverName.Equals("")) driverName = "未知设备";
                        break;
                }
                driverName = driverName + "(" + driver.Name.Substring(0, 2) + ")";
                driverTag = driver.Name;
                TreeNode driverTreeNode = new TreeNode(driverName);
                driverTreeNode.SelectedImageKey = driverTreeNode.ImageKey = keyName;
                driverTreeNode.Tag = driverTag;
                if (driver.IsReady)
                {//如果该驱动in position 查看是否有子目录，如有显示+
                    DirectoryInfo driver_info = new DirectoryInfo(driver.Name);
                    DirectoryInfo[] dirs = driver_info.GetDirectories();//获取元素
                    if (dirs.Length > 0) driverTreeNode.Nodes.Add("temp");
                }
                root.Nodes.Add(driverTreeNode);//添加子节点
            }
        }

        //单击“+”，展开其下一级子目录
        private static void treeView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            TreeNode tn = e.Node;

            //如果是我的电脑结点，则读取硬盘驱动器
            if (tn.Tag.Equals(Const.MYCOMPUTERPATH))
            {
                tn.Nodes.Clear();
                GetDriverTree(tn);
            }
            else //否则，如果不是收藏夹，则读取子目录，并显示在treeview中
                if (!tn.Tag.Equals(Const.FAVORITES))
            {
                tn.Nodes.Clear();
                GetFolderTree(tn);
            }


        }

        //根据节点代表的文件夹路径，获取子目录，并显示，tn是即将展开的结点
        private static void GetFolderTree(TreeNode tn)
        {
            string folderpath = tn.Tag.ToString();
            string[] f_names = Directory.GetDirectories(folderpath);
            foreach (string fn in f_names)
            {
                DirectoryInfo dinfo = new DirectoryInfo(fn);
                TreeNode newtn = new TreeNode(dinfo.Name);
                newtn.Tag = dinfo.FullName;//完整路径
                newtn.SelectedImageKey = newtn.ImageKey = Const.DEFAULTFOLDER;
                try
                {
                    //判断是否有子目录，有则添加临时子节点，以便显示“+”号
                    string[] temps = Directory.GetDirectories(fn);
                    if (temps.Length > 0) newtn.Nodes.Add("temp");
                }
                catch { }
                tn.Nodes.Add(newtn);
            }
        }

        public static void AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode tn = e.Node;

            
            Console.WriteLine(tn.Text + " selected . this node selectedImageKey=  " + tn.SelectedImageKey + 
                "    this node imageKey= " +tn.ImageKey + "this node selectedImageIndex = " + tn.SelectedImageIndex);

            ListViewControler.updateListViewByPath(tn.Tag.ToString().Trim());

            AccessPathHistory.getAccessPathHistoryObject().addHistory(tn.Tag.ToString().Trim());

            Combo_urlControler.getCombo_urlControlerObject(null).updateItems(AccessPathHistory.getAccessPathHistoryObject().getAccressHistory());

        }
    }
}
