﻿using System.Collections;

namespace zhuxunzui_WindowsFileManager.main.core
{
    class AccessPathHistory {
        private string currentPath;
        private static AccessPathHistory oneLive;
        private ArrayList accessPaths;

        public AccessPathHistory() {
            accessPaths = new ArrayList();
            currentPath = zhuxunzui_WindowsFileManager.main.util.Const.MYCOMPUTERPATH;
        }

        public static AccessPathHistory getAccessPathHistoryObject(){
            if (oneLive == null)
                oneLive = new AccessPathHistory();
            return oneLive;
        }

        public ArrayList getAccressHistory() { return accessPaths; }

        /// <summary> 想访问历史添加访问路径，并设置为当前路径 </summary>
        /// <param name="path" type="string">路径</param>
        /// <returns>void</returns>
        public void addHistory(string path) {
            if (accessPaths.IndexOf(path) >= 0)
                accessPaths.Remove(path);
            accessPaths.Add(path);
            currentPath = path;
        }

        /// <summary> 获取前一个访问路径 </summary>
        /// <returns type="string">返回前一个访问路径, 若当前路径为第一个则返回当前路径</returns>
        public string preAccessPath() {
            int idx = accessPaths.IndexOf(currentPath);
            if (idx > 0)
                return currentPath = (string)accessPaths[idx-1];

            return currentPath;
        }

        /// <summary> 获取后一个访问路径 </summary>
        /// <returns type="string">返回当前的后一个访问路径, 若当前路径为最后一个则返回当前路径</returns>
        public string nextAccessPath() {
            int idx = accessPaths.IndexOf(currentPath);
            if (idx < accessPaths.Count - 1)
                return currentPath = (string)accessPaths[idx + 1];
            return currentPath;
        }

        public int getCurrentPathIndex() {
            return accessPaths.IndexOf(currentPath);
        }
    }
}
