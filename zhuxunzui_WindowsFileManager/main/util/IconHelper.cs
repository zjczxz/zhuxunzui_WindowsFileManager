﻿using JHR_GetIcon;
using System.Drawing;
using System.Windows.Forms;
namespace zhuxunzui_WindowsFileManager.main.util
{
    class IconHelper {
        private static ImageList imagelist_s = new ImageList();
        private static ImageList imagelist_b = new ImageList();
        private static bool flag = false;

        private static void checkInit() {
            if (flag == true)
                return;
            imagelist_s.ImageSize = new Size(16, 16);
            imagelist_b.ImageSize = new Size(32, 32);
            string [] mykeys = { Const.COMPUTER,  Const.DESKTOP,Const.FAVORITES,Const.LOCALDRIVER,
                                  Const.CDROM,Const.MOVABLEDRIVER
                                  ,Const.RECYCLE,Const.DEFAULTFOLDER,Const.DEFAULTEXEICON,
                                  Const.UNKNOWICON,Const.PRINTER,Const.NETWORK
                              };
            int[] myindex = { 15, 34, 43, 8, 11, 7, 101, 4, 2, 0, 16, 17 };
            Icon [] myicon;
            GetIcon geticon = new GetIcon();

            for (int i = 0; i < myindex.Length; i++) {
                myicon = geticon.GetIconByIndex(myindex[i]);
                imagelist_b.Images.Add(mykeys[i], myicon[1]);
                imagelist_s.Images.Add(mykeys[i], myicon[0]);
            }

            flag = true;

        }

        public static void setIcon(string key, Icon[] myicon)
        {
            checkInit();
            imagelist_b.Images.Add(key, myicon[1]);
            imagelist_s.Images.Add(key, myicon[0]);
        }

        public static ImageList getImage_S() { return imagelist_s;}
        public static ImageList getImage_B() {return  imagelist_b;}

    }
}
