﻿namespace zhuxunzui_WindowsFileManager.main.util
{
    class Const {
        public const string MYCOMPUTERPATH = "This PC";


        public const string COMPUTER = "computer";
        public const string	DESKTOP = "desktop";
        public const string FAVORITES = "favorites";
        public const string	LOCALDRIVER = "localdriver";
        public const string	CDROM = "cdrom";
        public const string MOVABLEDRIVER = "movabledriver";
        public const string	RECYCLE = "recycle";
        public const string	DEFAULTFOLDER = "defalutfolder";
        public const string	DEFAULTEXEICON = "defaultexeicon";
        public const string	UNKNOWICON = "unknowicon";
        public const string	PRINTER = "printer";
        public const string	NETWORK = "network";
        public const string MYDOCUMENT = "mydocument";
        public const string MYMUSIC = "mymusic";
        public const string MYPICTURES = "mypictures";
        public const string MYVIDEOS = "myvideos";


        public enum ContextType {
            [System.ComponentModel.Description("特殊文件夹中的空白处")]
            specialEmpty = 0,
            [System.ComponentModel.Description("特殊文件夹中的选中")]
            specialItems = 1,
            [System.ComponentModel.Description("普通文件夹中的空白处")]
            CommonEmpty = 2,
            [System.ComponentModel.Description("普通文件夹中的选中")]
            CommonItes = 3,
            [System.ComponentModel.Description("回收站文件夹中的空白处")]
            RecycleEmpty = 4,
            [System.ComponentModel.Description("回收站文件夹中的选中")]
            RecycleItes = 5,
        }
    }
}
