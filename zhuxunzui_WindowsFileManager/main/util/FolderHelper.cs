﻿using System.IO;
using Microsoft.WindowsAPICodePack.Shell;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using System;
using System.Linq;
namespace zhuxunzui_WindowsFileManager.main.util
{
    class FolderHelPer
    {
        public static long totalSize(string path)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            DirectoryInfo[] infos = directoryInfo.GetDirectories();
            FileInfo[] fileInfos = directoryInfo.GetFiles();
            
            long size = 0;
            foreach (FileInfo f in fileInfos)
            {
                size += f.Length;
            }

            foreach (DirectoryInfo dirinf in infos)
            {
                size += totalSize(dirinf.FullName);
            }
            return size;
        }

        public static string getSizeString(string path)
        {
            long sizes = totalSize(path);
            string size;
            if (sizes < 1024) size = sizes.ToString() + " Byte";
            else size = (sizes / 1024).ToString() + " KB";
            return size;
        }

        public static string changeFileSizeFormatToString(long size)
        {
            string sizestring = "";
            if (size < 1024) sizestring = size.ToString() + " Byte";
            else sizestring = (size / 1024).ToString() + " KB";
            return sizestring;
        }

        public static string getFileSizeString(string path)
        {
            return "";
        }

        public static string getFileDisplayName(string fullName) {
            string res = "";
            ShellPropertyCollection cooll = new ShellPropertyCollection(fullName);
            var properties = cooll.Where(prop => prop.CanonicalName != null).OrderBy(prop => prop.CanonicalName).ToArray();
            Array.ForEach(
               properties,
               p =>
               {
                   //DisplayPropertyValue(p);
                   if (p.CanonicalName.Equals("System.FileName"))
                        res = (string)p.ValueAsObject;
               });

            return res;
        }
    }
}
