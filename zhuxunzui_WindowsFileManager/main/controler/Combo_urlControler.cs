﻿using System;
using System.Collections;
using System.Windows.Forms;
using zhuxunzui_WindowsFileManager.main.core;
namespace zhuxunzui_WindowsFileManager.main.controler
{
    class Combo_urlControler {
        private ComboBox combo_url;
        private static Combo_urlControler oneLive;


        public Combo_urlControler(ComboBox c) {
            combo_url = c;
        }


        public static Combo_urlControler getCombo_urlControlerObject(ComboBox c) {
            if (oneLive == null)
                oneLive = new Combo_urlControler(c);
            return oneLive;
        }

        public static void SelectedIndexChanged(object sender, EventArgs e) {
            //Console.WriteLine("combo_url selectChange");
            ListViewControler.updateListViewByPath(((ComboBox)sender).Text.Trim());

        }

        private void clearMethod() {
            getCombo_urlControlerObject(null).combo_url.SelectedIndexChanged -= new EventHandler(SelectedIndexChanged);
            
        }

        private void setMethod() {
            getCombo_urlControlerObject(null).combo_url.SelectedIndexChanged += new EventHandler(SelectedIndexChanged);
        }

        public void updateItems(ArrayList arraylist ) {
            clearMethod();
            combo_url.DataSource = null;
            //arraylist.Reverse();
            combo_url.DataSource = arraylist; //更新选项
            //combo_url.SelectedIndex = arraylist.Count - 1;
            combo_url.SelectedIndex = AccessPathHistory.getAccessPathHistoryObject().getCurrentPathIndex();

            setMethod();
        }
    }
}
