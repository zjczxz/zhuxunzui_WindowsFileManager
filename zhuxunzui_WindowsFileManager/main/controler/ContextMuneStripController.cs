﻿using JHR_GetIcon;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using zhuxunzui_WindowsFileManager.main.core;
using zhuxunzui_WindowsFileManager.main.util;
namespace zhuxunzui_WindowsFileManager.main.controler {
    class ContextMuneStripController {
        private static ContextMenuStrip contextMenuStrip;
        private static ListView listView;

        private static ArrayList copyobj = new ArrayList();

        private static Boolean isCut = false;

        public static void setContextMenuStrip(ContextMenuStrip c, ListView lv) {
            contextMenuStrip = c;
            listView = lv;
            c.Opening += new CancelEventHandler(openContextMenuStrip);

            listView.AfterLabelEdit += new LabelEditEventHandler(afterLabelEdit);

            for (int i = 0; i < contextMenuStrip.Items.Count; i++) {
                contextMenuStrip.Items[i].Click += new EventHandler(contextMenu_item_Click);
                for (int j = 0; j < ((ToolStripMenuItem)contextMenuStrip.Items[i]).DropDownItems.Count; j++) {
                    ((ToolStripMenuItem)contextMenuStrip.Items[i]).DropDownItems[j].Click += new EventHandler(contextMenu_item_Click);
                }
            }

        }

        private static void contextMenu_item_Click(object sender, EventArgs e) {
            //Console.WriteLine("hahahahha");
            ToolStripItem tsm = (ToolStripItem)sender;
            switch (tsm.Name) {
                case "tsm_copy":
                    docopy();
                    break;
                case "tsm_pause":
                    dopause();
                    break;
                case "tsm_cut":
                    docut();
                    break;
                case "tsm_del":
                    dodel();
                    break;
                case "tsm_rename":
                    dorename();
                    break;
                case "tsm_new_txt":
                    doNew("txt");
                    break;
                case "tsm_new_doc":
                    doNew("doc");
                    break;
                case "tsm_new_folder":
                    doNew("folder");
                    break;
                case "tsm_new_exc":
                    doNew("exc");
                    break;
                case "tsm_new_ppt":
                    doNew("ppt");
                    break;

                case "tsm_reflush":
                    doFlush();
                    break;
                case "tsm_info":
                    doInfo(listView.FocusedItem.Tag.ToString());
                    break;
                case "tsm_open":
                    doOpen();
                    break;
                case "tsm_return":
                    doReturn();
                    break;
                case "tsm_empty":
                    doEmpty();
                    break;
            }
        }

        private static void doFlush() {
            ListViewControler.updateListViewByPath((string)AccessPathHistory.getAccessPathHistoryObject().getAccressHistory()[AccessPathHistory.getAccessPathHistoryObject().getCurrentPathIndex()]);
        }

        private static void doOpen() {
            string path = listView.FocusedItem.Tag.ToString().Trim();
            if (File.Exists(path)) {
                try {
                    System.Diagnostics.Process.Start(path);
                }
                catch {
                    MessageBox.Show("无法打开或运行该文件", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                return;
            }
            ListViewControler.updateListViewByPath(path);
            AccessPathHistory.getAccessPathHistoryObject().addHistory(path);
            Combo_urlControler.getCombo_urlControlerObject(null).updateItems(AccessPathHistory.getAccessPathHistoryObject().getAccressHistory());
        }

        private static void doInfo(string fullName) {
            zxz_attr_form zxz_Attr = new zxz_attr_form(fullName);
            zxz_Attr.Show();
        }

        private static void docut() {
            if (listView.SelectedItems.Count == 0) return;
            isCut = true;
            copyobj.Clear();
            for (int i = 0; i < listView.SelectedItems.Count; i++) {
                copyobj.Add(listView.SelectedItems[i].Tag.ToString());
            }
        }

        private static void docopy() {
            if (listView.SelectedItems.Count == 0) return;
            isCut = false;
            copyobj.Clear();
            for (int i = 0; i < listView.SelectedItems.Count; i++) {
                copyobj.Add(listView.SelectedItems[i].Tag.ToString());
            }
        }

        private static void dopause() {
            if (copyobj.Count == 0) return;
            string currentPath = (string)AccessPathHistory.getAccessPathHistoryObject().getAccressHistory()[AccessPathHistory.getAccessPathHistoryObject().getCurrentPathIndex()];
            if (currentPath.Equals(Const.DESKTOP))
                currentPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            if (!Directory.Exists(currentPath))
                return;
            for (int i = 0; i < copyobj.Count; i++) {
                if (File.Exists(copyobj[i].ToString())) copyCutFile(copyobj[i].ToString(), currentPath);
                else if (Directory.Exists(copyobj[i].ToString())) copyCutDirectory(copyobj[i].ToString(), currentPath);
            }
            if (isCut)
                copyobj.Clear();
            isCut = false;
            ListViewControler.updateListViewByPath(currentPath);
        }

        private static void copyCutFile(string fullName, String tragetPath) {
            try {
                FileInfo fileInfo = new FileInfo(fullName);
                //string filename = fileInfo.Name;
                string filename = FolderHelPer.getFileDisplayName(fullName);

                string targetFullPath = Path.Combine(tragetPath, filename);
                DialogResult result = DialogResult.Yes;
                if (File.Exists(targetFullPath))
                    result = MessageBox.Show("文件 " + filename + "", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes) {
                    fileInfo.CopyTo(targetFullPath, true);
                    if (isCut == true)
                        File.Delete(fullName);
                }

            }
            catch (Exception ee) {
                MessageBox.Show(ee.Message);
            }
        }

        private static void copyCutDirectory(string fullName, String tragetPath) {
            try {
                DirectoryInfo directoryInfo = new DirectoryInfo(fullName);
                string dirName = FolderHelPer.getFileDisplayName(fullName); 

                //string dirName = directoryInfo.Name;
                string targetFullPath = Path.Combine(tragetPath, dirName);
                DialogResult result = DialogResult.Yes;
                if (Directory.Exists(targetFullPath)) {
                    result = MessageBox.Show("文件夹已存在，是否覆盖", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (result == DialogResult.Yes)
                        Directory.Delete(targetFullPath, true);
                    else return;
                }
                DirectoryInfo newDirectoryInfo = new DirectoryInfo(targetFullPath);
                newDirectoryInfo.Create();
                FileInfo[] files = directoryInfo.GetFiles();
                foreach (FileInfo file in files) {
                    file.CopyTo(Path.Combine(targetFullPath, file.Name), true);
                }
                DirectoryInfo[] directoryInfos = directoryInfo.GetDirectories();
                foreach (DirectoryInfo dir in directoryInfos) {
                    copyCutDirectory(dir.FullName, targetFullPath);
                }
                if (isCut) directoryInfo.Delete();
            }
            catch (Exception ee) {
                MessageBox.Show(ee.Message);
            }
        }


        private static void dodel() {
            string currentPath = (string)AccessPathHistory.getAccessPathHistoryObject().getAccressHistory()[AccessPathHistory.getAccessPathHistoryObject().getCurrentPathIndex()];
            if(currentPath.Equals(Const.RECYCLE)){
                doRecyleDel();
                return ;
            }

            try {
                if (listView.SelectedItems.Count == 0) return;
                for (int i = 0; i < listView.SelectedItems.Count; i++) {
                    string fullNmae = listView.SelectedItems[i].Tag.ToString();
                    if (File.Exists(fullNmae)) {
                        FileSystem.DeleteFile(fullNmae, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin);
                    } else if (Directory.Exists(fullNmae))
                        FileSystem.DeleteDirectory(fullNmae, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin);
                    else MessageBox.Show(fullNmae + "删除失败", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            catch (Exception ee) { MessageBox.Show(ee.Message); }

            ListViewControler.reflushListView(null, null);//刷新
        }
        private static void dorename() {
            if (listView.SelectedItems.Count != 1)
                return;
            string fullName = listView.SelectedItems[0].SubItems[0].Text;
            if (fullName.Equals(Const.MYCOMPUTERPATH) ||
                fullName.Equals(Const.NETWORK)
                || fullName.Equals(Const.RECYCLE)
                || fullName.Equals(Const.MYDOCUMENT)
                )
                return;

            listView.LabelEdit = true;
            listView.SelectedItems[0].BeginEdit();


            //listView.LabelEdit = false;
        }

        private static void afterLabelEdit(object sender, LabelEditEventArgs e) {
            if (e.Label == null ||e.Label.Trim() == "" || e.Label.Trim().Equals(listView.Items[e.Item].SubItems[0].Text.Trim()))
            {
                e.CancelEdit = true;
                listView.LabelEdit = false;
            }
            else
            {
                string currentPath = (string)AccessPathHistory.getAccessPathHistoryObject().getAccressHistory()[AccessPathHistory.getAccessPathHistoryObject().getCurrentPathIndex()];
                string newName = e.Label.Trim();
                try
                {
                    if (File.Exists(listView.Items[e.Item].Tag.ToString()))
                    {
                        if (File.Exists(Path.Combine(currentPath, newName)))
                        {
                            MessageBox.Show("文件名已存在，请重试", "错误", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                            e.CancelEdit = true;
                        }
                        else
                        {
                            File.Move(listView.Items[e.Item].Tag.ToString(), Path.Combine(currentPath, newName));
                            listView.Items[e.Item].Tag = Path.Combine(currentPath, newName);
                        }
                    }
                    else
                    {
                        if (Directory.Exists(listView.Items[e.Item].Tag.ToString()))
                        {
                            if (Directory.Exists(Path.Combine(currentPath, newName)))
                            {
                                MessageBox.Show("文件名已存在，请重试", "错误", MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                                e.CancelEdit = true;
                            }
                            else
                            {
                                Directory.Move(listView.Items[e.Item].Tag.ToString(), Path.Combine(currentPath, newName));
                                listView.Items[e.Item].Tag = Path.Combine(currentPath, newName);
                            };
                        }
                    }
                }
                catch (Exception ee) { MessageBox.Show(ee.Message); }
            }
            listView.LabelEdit = false;
        }

        private static void doNew(string type) {
            string newName = "";
            string newext = "";
            switch (type) {
                case "folder":
                    newName = "新建文件夹";
                    newext = "";
                    break;
                case "doc":
                    newName = "新建word文档"; newext = ".doc";
                    break;
                case "txt":
                    newName = "新建文本文档"; newext = ".txt";
                    break;
                case "exc":
                    newName = "新建EXCEL文档"; newext = ".xls";
                    break;
                case "ppt":
                    newName = "新建PPT演示文稿"; newext = ".ppt";
                    break;

            }
            string currentPath = (string)AccessPathHistory.getAccessPathHistoryObject().getAccressHistory()[AccessPathHistory.getAccessPathHistoryObject().getCurrentPathIndex()];
            try {
                ListViewItem listVewItem = new ListViewItem();
                int i = 1;
                string temp = newName;
                string typeName = "";
                if (type.Equals("folder")) {
                    while (Directory.Exists(Path.Combine(currentPath, newName))) newName = temp + i++.ToString();
                    Directory.CreateDirectory(Path.Combine(currentPath, newName));
                    listVewItem.Tag = Path.Combine(currentPath, newName);
                    listVewItem.ImageKey = Const.DEFAULTFOLDER;
                    typeName = "文件夹";
                } else {
                    while (File.Exists(Path.Combine(currentPath, newName+newext))) newName = temp + i++.ToString();
                    newName += newext;
                    File.Create(Path.Combine(currentPath, newName));
                    listVewItem.ImageKey = ListViewControler.GetFileIconKey(newext, Path.Combine(currentPath, newName));
                    typeName = new GetIcon().GetTypeName(Path.Combine(currentPath, newName));
                }
                listVewItem.Text = newName;
                listVewItem.Tag = Path.Combine(currentPath, newName);

                listVewItem.IndentCount = 1;
                listVewItem.SubItems.Add(typeName);
                listVewItem.SubItems.Add(DateTime.Now.ToString());
                listVewItem.SubItems.Add("");
                listVewItem.SubItems.Add(DateTime.Now.ToString());

                listView.Items.Add(listVewItem);
                listView.Items[listView.Items.Count - 1].Selected = true;

            }
            catch (Exception ee) { MessageBox.Show(ee.Message); }
            ListViewControler.updateListViewByPath(currentPath);
        }

        //TODO: 回收站还原
        private static void doReturn() {
            try {
                if (listView.SelectedItems.Count == 0) return;
                isCut = true;
                for (int i = 0; i < listView.SelectedItems.Count; i++) {
                    string[] currentNameAndOrignName = (string[])listView.SelectedItems[i].Tag;
                    string fullname = currentNameAndOrignName[0];
                    string orignName = currentNameAndOrignName[1];
                    if (File.Exists(fullname)) {
                        //FileSystem.DeleteFile(fullname, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin);
                        copyCutFile(fullname, orignName);
                    } else if (Directory.Exists(fullname))
                        copyCutDirectory(fullname, orignName);
                    else MessageBox.Show(fullname + "还原失败,", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                isCut = false;

            }
            catch (Exception ee) { MessageBox.Show(ee.Message); }
            ListViewControler.updateListViewByPath((string)AccessPathHistory.getAccessPathHistoryObject().getAccressHistory()[AccessPathHistory.getAccessPathHistoryObject().getCurrentPathIndex()]);
        }

        private static void doRecyleDel() {
            try {
                if (listView.SelectedItems.Count == 0) return;
                for (int i = 0; i < listView.SelectedItems.Count; i++) {
                    string fullname = ((string [])listView.SelectedItems[i].Tag)[0];
                    if (File.Exists(fullname)) {
                        FileSystem.DeleteFile(fullname, UIOption.OnlyErrorDialogs, RecycleOption.DeletePermanently);
                    } else
                        if (Directory.Exists(fullname))
                            FileSystem.DeleteDirectory(fullname, UIOption.OnlyErrorDialogs, RecycleOption.DeletePermanently);
                        else MessageBox.Show(fullname + ",删除失败", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }

            }
            catch (Exception ee) { MessageBox.Show(ee.Message); }
            ListViewControler.updateListViewByPath((string)AccessPathHistory.getAccessPathHistoryObject().getAccressHistory()[AccessPathHistory.getAccessPathHistoryObject().getCurrentPathIndex()]);

        }

        private static void doEmpty() {
            try {
                for (int i = 0; i < listView.Items.Count; i++) {
                    string fullname = ((string[])listView.Items[i].Tag)[0];
                    if (File.Exists(fullname)) {
                        FileSystem.DeleteFile(fullname, UIOption.OnlyErrorDialogs, RecycleOption.DeletePermanently);
                    } else
                        if (Directory.Exists(fullname))
                            FileSystem.DeleteDirectory(fullname, UIOption.OnlyErrorDialogs, RecycleOption.DeletePermanently);
                        else MessageBox.Show(fullname + "，删除失败", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            catch (Exception ee) { MessageBox.Show(ee.Message); }
            ListViewControler.updateListViewByPath((string)AccessPathHistory.getAccessPathHistoryObject().getAccressHistory()[AccessPathHistory.getAccessPathHistoryObject().getCurrentPathIndex()]);
        }
        public static void openContextMenuStrip(object sender, CancelEventArgs e) {
            string currentPath = (string)AccessPathHistory.getAccessPathHistoryObject().getAccressHistory()[AccessPathHistory.getAccessPathHistoryObject().getCurrentPathIndex()];

            if (listView.SelectedItems.Count == 0) {
                if (currentPath.Equals(Const.MYCOMPUTERPATH) ||
                    currentPath.Equals(Const.FAVORITES)) {

                    setItemDisplay(Const.ContextType.specialEmpty);
                } else if (currentPath.Equals(Const.RECYCLE)) {
                    setItemDisplay(Const.ContextType.RecycleEmpty);
                } else {
                    setItemDisplay(Const.ContextType.CommonEmpty);
                }
            } else {
                if (currentPath.Equals(Const.MYCOMPUTERPATH) ||
                    currentPath.Equals(Const.FAVORITES)) {

                    setItemDisplay(Const.ContextType.specialItems);
                } else if (currentPath.Equals(Const.RECYCLE)) {
                    setItemDisplay(Const.ContextType.RecycleItes);
                } else {
                    setItemDisplay(Const.ContextType.CommonItes);
                }
            }
        }


        //TODO:  修改菜单显示条目 和 显示的 不同情况
        public static void setItemDisplay(Const.ContextType type) {

            for (int i = 0; i < contextMenuStrip.Items.Count; i++) {
                contextMenuStrip.Items[i].Enabled =
                    ((int.Parse(contextMenuStrip.Items[i].Tag.ToString().Trim()) >> (int)type) & 1) > 0
                    ? true : false;
                contextMenuStrip.Items[i].Visible = ((int.Parse(contextMenuStrip.Items[i].Tag.ToString().Trim()) >> (int)type) & 1) > 0
                    ? true : false;
            }
            contextMenuStrip.Items["tsm_pause"].Enabled = copyobj.Count > 0 ? true : false;
            contextMenuStrip.Items["tsm_pause"].Visible = copyobj.Count > 0 ? true : false;
        }
    }
}
