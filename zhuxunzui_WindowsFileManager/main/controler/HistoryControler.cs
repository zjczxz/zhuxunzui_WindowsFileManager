﻿using System;
using zhuxunzui_WindowsFileManager.main.core;
namespace zhuxunzui_WindowsFileManager.main.controler
{
    class HistoryControler {
        public static void preAccessPath(object sender, EventArgs e) {
            ListViewControler.updateListViewByPath(
                AccessPathHistory.getAccessPathHistoryObject().preAccessPath()
                );
            Combo_urlControler.getCombo_urlControlerObject(null).updateItems(AccessPathHistory.getAccessPathHistoryObject().getAccressHistory());

        }

        public static void nextAccessPath(object sender, EventArgs e) {
            ListViewControler.updateListViewByPath(
                AccessPathHistory.getAccessPathHistoryObject().nextAccessPath()
                );
            Combo_urlControler.getCombo_urlControlerObject(null).updateItems(AccessPathHistory.getAccessPathHistoryObject().getAccressHistory());

        }
    }
}
